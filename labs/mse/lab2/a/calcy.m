#import "calcy.h"

@implementation Calculator
@synthesize x = x;
@synthesize y = y;

- (double) add {
	return x + y;
}

- (double) sub {
	return x - y;
}

- (double) mul {
	return x * y;
}

- (double) div {
	return x / y;
}
@end