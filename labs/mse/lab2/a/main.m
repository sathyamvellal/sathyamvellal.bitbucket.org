#import <Foundation/Foundation.h>
#import "calcy.h"

void menu (int *choice)
{
	printf ("1. Add\n");
	printf ("2. Sub\n");
	printf ("3. Mul\n");
	printf ("4. Div\n");
	printf ("5. Exit\n");
	printf ("Enter choice: ");

	scanf ("%d", choice);
}

void read (double *x, double *y)
{
	printf ("Enter two numbers:\n");
	while (scanf ("%lf", x), *x == 0) {
		printf ("Enter a non-zero value: ");
	}
	while (scanf ("%lf", y), *y == 0) {
		printf ("Enter a non-zero value: ");
	}
}

int main ()
{
	Calculator* c;
	c = [[Calculator alloc] init];
	int choice;
	double x; double y;

	do {
		menu (&choice);
		if (choice != 5) {
			read (&x, &y);
			[c setX:x]; [c setY:y];
		}

		switch (choice) {
		case 1:
			printf ("Result: %lf\n", [c add]);
		break;
		case 2:
			printf ("Result: %lf\n", [c sub]);
		break;
		case 3:
			printf ("Result: %lf\n", [c mul]);
		break;
		case 4:
			printf ("Result: %lf\n", [c div]);
		break;
		case 5:
		break;
		default:
			printf ("Invalid choice. Please enter again\n");
			menu (&choice);
		}
		printf ("\n");
	} while (choice != 5);

	return 0;
}