#import <Foundation/Foundation.h>

@protocol Item<NSObject>
- (double) getPricePerUnit;
- (double) getQuantity;
- (id) getTaxType;
- (id) getBill;
@end

@interface FinishedItem : NSObject<Item>
{
	@private double quantity;
	@private double pricePerUnit;
}
- (id) initWithPricePerUnit:(double)ppu initWithQuantity:(double)amount;
@end

@interface GroceryItem : NSObject<Item>
{
	@private double quantity;
	@private double pricePerUnit;
}
- (id) initWithPricePerUnit:(double)ppu initWithQuantity:(double)amount;
@end

@interface Chair : FinishedItem
@end

@interface Table : FinishedItem
@end

@interface Rice : GroceryItem
@end

@interface Wheat : GroceryItem
@end

