Lab 3 - 20-08-2013
==================

Write an object C program to compute the tax on a given bill.  
The application should compute (KST and CST) or VAT based on the bill type.  
* If the bill type is "finished/export goods", compute KST and CST (14% and 4% resp.
* If the bill type is "grocery", compute VAT (8%)

Output the total tax amount and the total bill amount.
